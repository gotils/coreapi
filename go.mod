module gitlab.com/gotils/coreapi

go 1.15

require (
	github.com/pkg/errors v0.8.1
	gitlab.com/gotils/corectx v1.0.0
)
